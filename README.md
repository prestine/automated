# Automated
**This is a simple dockerized node app made for testing purposes, more will be added soon**


## About:
Automated the creation of 2 VMs through Vagrant and Ansible, one responsible for the building stage of the app and the other for deployment stage with CI/CD.
